# Comandos docker

### Detener contenedores

`docker stop $(docker ps -a -q)`

### Eliminar contenedores

`docker rm $(docker ps -a -q)`

### Eliminar imágenes

`docker rmi -f $(docker images -q)`

### Ejecutar docker-compose

`docker-compose up -d`

Para que pueda acceder a la base de datos desde un gestor de base de datos tiene que habilitar la opción `allowPublicKeyRetrieval = true`
